#define FREEGLUT_STATIC
#include <GL/freeglut.h>
#include <math.h>
#define MAX_CHAR 128

// window size
float width = 1000.0, height = 600.0;

//scaling factor for stars
//float s = 1.0;

// refresh interval in ms
int time_interval = 16;

// force to redraw
void when_in_mainloop() {
    glutPostRedisplay();
}

// text
void drawText(const char* str) {
    static int isFirstCall = 1;
    static GLuint lists;
    if (isFirstCall) {
        isFirstCall = 0;
        lists = glGenLists(MAX_CHAR);
        wglUseFontBitmaps(wglGetCurrentDC(), 0, MAX_CHAR, lists);
    }
    for (; *str != '\0'; ++str) {
        glCallList(lists + *str);
    }
}
void selectFont(int size, int charset, const char* face) {
    HFONT hFont = CreateFontA(size, 0, 0, 0, FW_MEDIUM, 0, 0, 0, charset, OUT_DEFAULT_PRECIS,
        CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FIXED_PITCH | FF_SWISS, face);
    HFONT hOldFont = (HFONT)SelectObject(wglGetCurrentDC(), hFont);
    DeleteObject(hOldFont);
}


void OnTimer(int value) {
    glutTimerFunc(time_interval, OnTimer, 1);
}
void displayCard();


// draw filled circle
void drawCircle(float r, float x, float y) {
    float i = 0.0f;
    glVertex2f(x, y); // Center of circle
    glBegin(GL_TRIANGLE_FAN);
    for (i = 0.0f; i <= 360; i++) {
        glVertex2f(r * cos(3.1416 * i / 180.0) + x, r * sin(3.1416 * i / 180.0) + y);
    }
    glEnd();      
}

void Snowman(float xd) {
    //snwoman body
    glColor3f(1, 1, 1); //white
    float heady = 40.0;
    float headR = 15.0;
    //head circle
    drawCircle(headR, xd, heady);
    //body circle
    drawCircle(1.5f*headR, xd, heady-headR-0.25f*heady);

    // rotated hat
    glPushMatrix();
    glTranslatef(xd, heady, 0);
    glRotatef(-30.0, 0.0, 0.0, 1.0);
    glTranslatef(-xd, -heady, 0);
    // hat
    glBegin(GL_TRIANGLES);
        glColor3f(1, 0, 0); // red
        glVertex2f(xd - 10, heady+headR);
        glVertex2f(xd +10, heady+headR);
        glVertex2f(xd, (heady+ 1.7*headR));
    glEnd();
    glPopMatrix();

    //eyes
    glColor3f(0, 0, 0); // black
    drawCircle(2.0, xd - 0.5 * headR, 1.1 * heady);
    drawCircle(2.0, xd + 0.5 * headR, 1.1 * heady);

    //hands
    glColor3f(126.0 / 255.0, 96.0 / 255.0, 0.0); // brown
    glBegin(GL_LINES);
        // body+R
        glVertex2f(xd + 1.5f * headR, heady - headR - 0.25f * heady);
        glVertex2f(xd + 30, heady);
    glEnd();
}

// tie
void tie(float xd) {
    glMatrixMode(GL_MODELVIEW);      // To operate on Model-View matrix
    glLoadIdentity();
    glTranslatef(xd + 45.0, 60.0, 0.0); 
    // right tie
    glBegin(GL_TRIANGLES);               
    glColor3f(255.0 / 255.0, 228.0 / 255.0, 111.0 / 255.0); //yellow
    glVertex2f(0, 0);     
    glVertex2f(5.0, 10.0);
    glVertex2f(7.0, 7.0);
    glEnd();

    // left tie
    glBegin(GL_TRIANGLES);           
    glVertex2f(0, 0);
    glVertex2f(-5.0, 10.0);
    glVertex2f(-7.0, 7.0);
    glEnd();

    glLoadIdentity(); // clear matrix
}

void giftbox(float xd) {
    //box
    glColor3f(1.0, 30.0/255.0, 0.0); // red
    glBegin(GL_POLYGON);
        glVertex2f(xd + 30.0, 30.0);
        glVertex2f(xd + 30.0, 60.0);
        glVertex2f(xd + 60.0, 60.0);
        glVertex2f(xd + 60.0, 30.0);
    glEnd();

    // x mid point of box
    float x_mid = xd + 45.0;

    //bow
    glColor3f(1.0, 228.0 / 255.0, 111.0 / 255.0); // yellow 
    glBegin(GL_POLYGON);
    glVertex2f(x_mid-5.0, 31.0);
    glVertex2f(x_mid - 5.0, 60.0);
    glVertex2f(x_mid+5.0, 60.0);
    glVertex2f(x_mid + 5.0, 31.0);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex2f(xd + 30.0, 40.0);
    glVertex2f(xd + 30.0, 50.0);
    glVertex2f(xd + 60.0, 50.0);
    glVertex2f(xd + 60.0, 40.0);
    glEnd();

    //tie
    tie(xd);
}

void heart(float r, float x, float y) {
    double i;
    glColor3f(233.0 / 255.0, 22.0 / 255.0, 136.0 / 255.0); // pink 
    glBegin(GL_TRIANGLE_FAN);
    // draw cardioid
    for (i = 0.0f; i <= 360; i++) {
        glVertex2f(16 * pow(sin(i), 3)+x, 
            (13 * cos(i)) - (5 * cos(2 * i)) - (2 * cos(3 * i)) - (cos(4 * i))+y);
    }
    glEnd();
}

void stars() {
    glColor3f(1.0, 1.0, 1.0); // white
    drawCircle(2.0f, 130, 420);
    drawCircle(2.0f, 200, 320);
    drawCircle(2.0f, 180, 467);
    drawCircle(2.0f, 270, 400);
    drawCircle(2.0f, 50, 411);
    drawCircle(2.0f, 89, 480);
    drawCircle(2.0f, 100, 450);
    drawCircle(2.0f, 20, 400);
    drawCircle(2.0f, 255, 500);
    drawCircle(2.0f, 275, 550);
    drawCircle(2.0f, 100, 520);
    drawCircle(2.0f, 130, 560);
}

void sky() {
    //full moon
    glColor3f(1, 1, 1); // white
    drawCircle(50.0, 0.1f * width, (5.0 / 6.0) * height);

    //waning crescent
    glColor3f(17.0 / 255.0f, 29.0 / 255.0f, 111.0 / 255.0); // background color
    glPushMatrix();
    glTranslatef(20.0, 20.0, 0);
    drawCircle(50.0, 100.0, 500.0);
    glPopMatrix();

    /*
    
    // shining stars
    glPushMatrix();
    //glScalef(s, s, 0);
    //random stars around the moon
    int i = 0;
    glColor3f(1.0, 1.0, 1.0);
    for (i = 0; i < 12; i += 1) {
        //drawCircle(s, rand() % 300, (rand() % 100) + 400.0);
        drawCircle(2.0f, rand() % 300, (rand() % 100)+400.0);
    }
    glPopMatrix();
    */

}

/*
void LostControlStars() {
    glPushMatrix();
    glScalef(s, s, 0);
    //random stars around the moon
    int i = 0;
    glColor3f(1.0, 1.0, 1.0);
    for (i = 0; i < 12; i += 1) {
        drawCircle(s, rand() % 400, (rand() % 100) + 400.0);
        //drawCircle(2.0f, rand() % 300, (rand() % 100) + 400.0);
    }
    glPopMatrix();
}
*/
void trees() {
    // tree trunk
    glColor3f(126.0/255.0, 96.0 / 255.0, 0.0); // green
    glBegin(GL_POLYGON);
        glVertex2f(910.0, 0.0);
        glVertex2f(910.0, 50.0);
        glVertex2f(890.0, 50.0);
        glVertex2f(890.0, 0.0);
    glEnd();

    float bottomP = 100.0;
    float bottomL = 850.0;
    float bottomR = 950.0;
    float bottomH = 50.0;

    // bottom leaves
    glColor3f(0.0, 121.0 / 255.0, 48.0 / 255.0);

    glBegin(GL_TRIANGLES);
        glVertex2f(bottomL, 50.0);
        glVertex2f(bottomR, 50.0);
        glVertex2f(900.0, bottomP);
    glEnd();

    // snow on side
    glColor3f(1.0, 1.0, 1.0);
    glLineStipple(5, 0x5555);
    glEnable(GL_LINE_STIPPLE);
    glBegin(GL_LINES);
    glVertex2f(bottomL, 50.0);
    glVertex2f(900.0, bottomP);
    glEnd();
    glDisable(GL_LINE_STIPPLE);

    // other leaves 
    glColor3f(0.0, 121.0 / 255.0, 48.0 / 255.0);
    glBegin(GL_TRIANGLES);
    glVertex2f(bottomL+15.0, 50.0 + bottomH - 10.0);
    glVertex2f(bottomR -15.0, 50.0 + bottomH - 10.0);
    glVertex2f(900.0, 50.0 + bottomH +20.0);
    glEnd();

    glBegin(GL_TRIANGLES);
    glVertex2f(bottomL + 30.0, 50.0 + bottomH + 15.0);
    glVertex2f(bottomR - 30.0, 50.0 + bottomH + 15.0);
    glVertex2f(900.0, 50.0 + bottomH +40.0);
    glEnd();
    
    // snow on top 
    glColor3f(1.0, 1.0, 1.0);
    glLineStipple(5, 0x5555);
    glEnable(GL_LINE_STIPPLE);
    glBegin(GL_LINES);
    glVertex2f(bottomR - 30.0, 50.0 + bottomH + 15.0);
    glVertex2f(900.0, 50.0 + bottomH + 40.0);
    glEnd();
    glDisable(GL_LINE_STIPPLE);

}


bool draw_snowman = false;
bool disp_instruc = true, disp_greeting = true;
bool gifttext = false, endText = false;

// keyboard interactions
void keyboard_input(unsigned char key, int x, int y) {
    if (key == 'q' || key == 'Q') 
        exit(0);
    else if (key == 'S' || key == 's') {
        draw_snowman = true;
        gifttext = true;
        disp_instruc = false;
    }
    glutPostRedisplay();
}

bool draw_giftbox = false, draw_heart = false;

int count = 0;
// mouse interactions
void mouse_input(int button, int state, int x, int y) {
    if (state == GLUT_DOWN && 
        (button == GLUT_LEFT_BUTTON || button == GLUT_RIGHT_BUTTON|| button == GLUT_MIDDLE_BUTTON)
        && draw_snowman == true) {
        count++;
        if (count == 1) {
            draw_giftbox = true;
            // gift box appear
        }
        else if (count == 2) {
            draw_giftbox = false;
            draw_heart = true;
            // gift box disappear and heart shows up
        }
        else if (count == 3) {
            disp_greeting = false;
            // initial greeting disappear and new wishes come up
        }
        else if (count == 4) {
            disp_greeting = true;
            gifttext = false;
            endText = true;
        }
    }
}

void instruction() { // instruction text
    glColor3f(255.0 / 255.0, 228.0 / 255.0, 111.0 / 255.0); // grey 
    glRasterPos2f(300.0, 50);
    drawText("Press 'S' call Snowman");
}

void greetings(float leftx, float topy) {
    // Draw 2D text
    glMatrixMode(GL_MODELVIEW);      // To operate on Model-View matrix
    glLoadIdentity();
    glTranslatef(leftx, topy, 0.0);

    glColor3f(233.0 / 255.0, 22.0 / 255.0, 136.0 / 255.0);
    selectFont(50, ANSI_CHARSET, "Comic Sans MS ITALIC");
    glRasterPos2f(0.0, 0.0);
    drawText("HAPPY NEW YEAR");

    glColor3f(255.0 / 255.0, 228.0 / 255.0, 111.0 / 255.0);
    glRasterPos2f(100, -50.0);
    drawText("2023");

    glColor3f(0.0, 1.0, 1.0);
    glRasterPos2f(10, -100);
    drawText("BEST WISHES");

    glLoadIdentity(); // clear matrix
}

void callgift() {
    glColor3f(236.0/255.0, 236.0 / 255.0, 236.0 / 255.0);
    glRasterPos2f(200, 100.0);
    drawText("CLICK FOR GIFT AND WISHES! ");
}

void wishes() {
    glColor3f(255.0 / 255.0, 228.0 / 255.0, 111.0 / 255.0);
    glRasterPos2f(320, 350);
    drawText("Have a sparkling New Year!");
    glRasterPos2f(220, 280);
    drawText("May the new year bless you");
    glRasterPos2f(90, 210);
    drawText("with health, wealth, and happiness!");
}

void ending() { // ending text
    glColor3f(236.0 / 255.0, 236.0 / 255.0, 236.0 / 255.0);
    glRasterPos2f(100, 100.0);
    drawText("Press Q to exit");
    //glRasterPos2f(100, 50.0);
    //drawString("Press A for fun :)");
}

void displayCard()
{
    //set projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // projection parameters
    gluOrtho2D(0, width, 0, height);

    //Background Color
    glClearColor(17.0/255.0f, 29.0 / 255.0f, 111.0 / 255.0, 1); // dark blue
    glClear(GL_COLOR_BUFFER_BIT);

    sky();
    stars();
    trees();

    // copy small trees
    glPushMatrix();
    glScalef(0.9, 0.8, 0.8);
    trees();
    glPopMatrix();

    
    if (draw_snowman) {
        Snowman(0.5*width);
    }
    if (draw_giftbox) {
        giftbox(0.5 * width);
    }
    if (draw_heart) {
        heart(15.0, 0.5 * width + 40.0, 40.0);
    }

    if (disp_greeting == true) {
        greetings(320, 350);
    }
    else {
        wishes();
    }

    if (disp_instruc) {
        instruction();
    }
    if (gifttext) {
        callgift();
    }
    if (endText) {
        ending();
    }

      //LostControlStars();
    
    glutSwapBuffers();

    glFlush();
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);

    glutInitWindowSize(width, height);
    glutInitWindowPosition(50, 50);
    glutCreateWindow("New Year Greeting Card");
    glutDisplayFunc(displayCard);

    glutIdleFunc(when_in_mainloop);
    glutTimerFunc(time_interval, OnTimer, 1);

    glutKeyboardFunc(keyboard_input);
    glutMouseFunc(mouse_input);
    
    glutMainLoop();
}