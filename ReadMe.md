# New Year Card

## Project Description
This project will generate a 2D interactive New Year Greeting card. 

**'PartialPreview' file will provide some images about how the program executed.**

Creation of Geometry, transformations, viewing, animation and interactions via mouse and keyboards are techniques used for this project.

**Library imported:** FreeGlut

**API:** OpenGL

**Programming Language** :C


